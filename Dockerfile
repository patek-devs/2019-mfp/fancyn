FROM golang:1.16-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY main.go ./
COPY controller ./controller
COPY display ./display

RUN go build -o /fancyn

EXPOSE 8080
EXPOSE 8081

CMD [ "/fancyn" ]